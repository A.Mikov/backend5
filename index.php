<?php
header('Content-Type: text/html; charset=UTF-8');
  
$messages = [];
$errors = [];
$trimed=[];

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

if (!empty($_COOKIE['save'])) {
  // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    // Выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
    // Если в куках есть пароль, то выводим сообщение.
    if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }

}

  $errors['name'] = !empty($_COOKIE['error_name']);
  $errors['email'] = !empty($_COOKIE['error_email']);
  $errors['bd'] = !empty($_COOKIE['error_bd']);
  $errors['pol'] = !empty($_COOKIE['error_pol']);
  $errors['limbs'] = !empty($_COOKIE['error_limbs']);
  $errors['contract'] = !empty($_COOKIE['error_contract']);

  
//Удаление cookies(через установление даты устаревания в прошедшем времени) и вывод сообщений об ошибках заполнения полей
if ($errors['name']) {
  setcookie('error_name', '', 100000);
  $messages[] = '<div ">Заполните имя.</div>';
}
if ($errors['email']) {
  setcookie('error_email', '', 100000);
  $messages[] = '<div ">Заполните почту.</div>';
}
if ($errors['bd']) {
  setcookie('error_bd', '', 100000);
  $messages[] = '<div ">Заполните др.</div>';
}
if ($errors['pol']) {
  setcookie('error_pol', '', 100000);
  $messages[] = '<div ">Заполните пол.</div>';
}
if ($errors['limbs']) {
  setcookie('error_limbs', '', 100000);
  $messages[] = '<div ">Заполните конечности.</div>';
}
if ($errors['contract']) {
  setcookie('error_contract', '', 100000);
  $messages[] = '<div ">Заполните контракт.</div>';
}


$values = array();
$values['name'] = empty($_COOKIE['name_value']) ? '' : strip_tags($_COOKIE['name_value']);
$values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
$values['bd'] = empty($_COOKIE['bd_value']) ? '' : strip_tags($_COOKIE['bd_value']);
$values['pol'] = empty($_COOKIE['pol_value']) ? '' : strip_tags($_COOKIE['pol_value']);
$values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : strip_tags($_COOKIE['limbs_value']);
$values['contract'] = empty($_COOKIE['contract_value']) ? '' : strip_tags($_COOKIE['contract_value']);
$values['superpowers'] = array();
$values['superpowers'][0] = empty($_COOKIE['superpowers_value_0']) ? '' : $_COOKIE['superpowers_value_0'];
$values['superpowers'][1] = empty($_COOKIE['superpowers_value_1']) ? '' : $_COOKIE['superpowers_value_1'];
$values['superpowers'][2] = empty($_COOKIE['superpowers_value_2']) ? '' : $_COOKIE['superpowers_value_2'];
$values['superpowers'][3] = empty($_COOKIE['superpowers_value_3']) ? '' : $_COOKIE['superpowers_value_3'];

// Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
// ранее в сессию записан факт успешного логина.
session_start();
if (empty($errors) && !empty($_COOKIE[session_name()]) && !empty($_SESSION['login'])) {
    // TODO: загрузить данные пользователя из БД
    // и заполнить переменную $values,
    // предварительно санитизовав.
    $user = 'u36993';
    $pass = '3932198';
    $db = new PDO('mysql:host=localhost;dbname=u36993', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

    $stmt1 = $db->prepare('SELECT name, email, bd, pol, limbs, bio FROM FORM WHERE user_id = ?');
    $stmt1->execute([$_SESSION['uid']]);
    $row = $stmt1->fetch(PDO::FETCH_ASSOC);
    $values['name'] = strip_tags($row['name']);
    $values['email'] = strip_tags($row['email']);
    $values['bd'] = strip_tags($row['bd']);
    $values['pol'] = strip_tags($row['pol']);
    $values['limbs'] = strip_tags($row['limbs']);
    $values['bio'] = strip_tags($row['bio']);

    $stmt2 = $db->prepare('SELECT id_sup FROM user_sups WHERE user_id = ?');
    $stmt2->execute([$_SESSION['uid']]);
    while($row = $stmt2->fetch(PDO::FETCH_ASSOC)) {
      $values['superpowers'][$row['id_sup']] = TRUE;
    }

    printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
}

// Включаем содержимое файла form.php.
// В нем будут доступны переменные $messages, $errors и $values для вывода 
// сообщений, полей с ранее заполненными данными и признаками ошибок.

include('form.php');
}

else {
  $errors = FALSE;

  //проверка корректности заполненных полей
  if ((empty($_POST['name']))) {
    setcookie('error_name', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
  }


  if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $_POST['email'])) {
    setcookie('error_email', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
  }
  
  
  if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $_POST['bd'])) {
    setcookie('error_bd', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('bd_value', $_POST['bd'], time() + 30 * 24 * 60 * 60);
  }
  

  if (!preg_match('/^[MFO]$/', $_POST['pol'])) {
    setcookie('error_pol', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('pol_value', $_POST['pol'], time() + 30 * 24 * 60 * 60);
  }


  if (!preg_match('/^[2-6]$/', $_POST['limbs'])) {
    setcookie('error_limbs', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
  }


  if (!isset($_POST['contract'])) {
    setcookie('error_contract', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('contract_value', $_POST['contract'], time() + 30 * 24 * 60 * 60);
  }


  foreach($_POST['superpowers'] as $super) {
    setcookie('superpowers_value_' . $super, 'true', time() + 30 * 24 * 60 * 60);
    }
 
  if ($errors) {
    header('Location: index.php');
    exit();
  }
  else {
    setcookie('error_name', '', 100000);
    setcookie('error_email', '', 100000);
    setcookie('error_pol', '', 100000);
    setcookie('error_limbs', '', 100000);
    setcookie('error_bd', '', 100000);
    setcookie('error_contract', '', 100000);
  }


    // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
    if (!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])) {
        // TODO: перезаписать данные в БД новыми данными,
        $user = 'u36993';
        $pass = '3932198';
        $db = new PDO('mysql:host=localhost;dbname=u36993', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

        $stmt1 = $db->prepare("UPDATE FORM SET name = ?, email = ?, bd = ?, pol= ? , kon = ?, bio = ?  WHERE user_id = ?");
        $stmt1 -> execute([$_POST['name'], $_POST['email'], $_POST['bd'], $_POST['pol'], $_POST['limbs'], $_POST['bio'], $_SESSION['uid']]);

        $stmt2 = $db->prepare('DELETE FROM user_sups WHERE user_id = ?');
        $stmt2->execute([$_SESSION['uid']]);

        $stmt3 = $db->prepare("INSERT INTO user_sups SET user_id = ?, id_sup = ?");
        foreach ($_POST['superpowers'] as $super) $stmt3 -> execute([$lastId, $super]);
    }
    else{
      // Генерируем уникальный логин и пароль.
      // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
      $id = uniqid();
      $hash = md5($id);
      $login = substr($hash, 0, 10);
      $pass = substr($hash, 10, 15);
      $hash_pass = substr(hash("sha256", $pass), 0, 20);
      // Сохраняем в Cookies.
      setcookie('login', $login);
      setcookie('pass', $pass);
  
       $user = 'u36993';
       $pass_db = '3932198';
       $db = new PDO('mysql:host=localhost; dbname=u36993', $user, $pass_db, array(PDO::ATTR_PERSISTENT => true));


       $stmt1 = $db->prepare("INSERT INTO FORM SET name = ?, email = ?, bd = ?, pol= ? , kon = ?, bio = ?, login = ?, hash_pass = ?");
       $stmt1 -> execute([$_POST['name'], $_POST['email'], $_POST['bd'], $_POST['pol'], $_POST['limbs'], $_POST['bio'], $login, $hash_pass]);
       $stmt2 = $db->prepare("INSERT INTO user_sups SET user_id = ?, id_sup = ?");
       $id = $db->lastInsertId();
       foreach ($_POST['superpowers'] as $super) $stmt2 -> execute([$id, $super]);
    }

  setcookie('save', '1');

  header('Location: ./');

}
